# ACP Control Time Calculator

This project calculates control open and close times for a given route. This calculator is based on Randonneurs USA regulations, and more information can be found [here](https://rusa.org/pages/acp-brevet-control-times-calculator). 

## Usage

First, set your brevet distance and start time. Then, for each desired control point, enter each distance in miles or kilometers, and the corresponding open and close times will be automatically calculated. 

## API

There is one GET endpoint that returns the start and end times for a given brevet: `/_calc_times`. The input takes the following parameters:

* `km`: Float, the number of kilometers for the current control location. Defaults to 0 km
* `start`: String, the start time in ISO 8601 format. Defaults to the current time
* `brevet`: Integer, the brevet distance. Defaults to 200 km

The following is returned:

* `open`: String, the control open time in ISO 8601 format
* `close`: String, the control close time in ISO 8601 format

## Assumptions

Calculations are based on rules specified by RUSA. This section explains some specifics about the implementation algorithm:

* For opening times, the control location is clamped to be between zero and the brevet distance
* For closing times, the control location is max'd to be at least zero. If the control point is farther than the brevet length, then it is assumed to be the end of the brevet, and separate calculations will occur instead
* Invalid data passed in will default to the default as specified in the API
