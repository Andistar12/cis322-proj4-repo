"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow
import math

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#

"""
The max speeds corresponding to brevet sections
Each tuple is of the form (range km, speed km/h) where each range
Builds off the last
Example: the third entry represents the range 600-1000 because there are three
200's before it, and the section range is 400
"""
max_speeds = [
    (200, 34),
    (200, 32),
    (200, 30),
    (400, 28),
    (300, 26)
]

"""
The min speeds corresponding to brevet sections
Each tuple is of the form (range km, speed km/h) where each range
Builds off the last
Example: the second entry represents the range 600-1000 because there is one
600 before it, and the section range is 400
"""
min_speeds = [
    (600, 15),
    (400, 11.428),
    (300, 13.333)
]

"""
The enforced final close times for brevets
Units are km: hours
"""
regulation_close_times = {
    200: 13.5,
    300: 20,
    400: 27,
    600: 40,
    1000: 75
}

def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    arrow_time = None
    try:
        arrow_time = arrow.get(brevet_start_time)
    except:
        arrow_time = arrow.now()
    if brevet_dist_km not in regulation_close_times:
        brevet_dist_km = 200 # Default to 200
    control_dist_km = max(0, min(control_dist_km, brevet_dist_km)) # Clamp
    delta_hrs, delta_mins = 0, 0 # Time addition measured in hours and minutes
    for ms in max_speeds:
        # (location range, speed)
        dist = min(ms[0], control_dist_km)
        dm, dh = math.modf(dist / ms[1])
        delta_hrs += dh
        delta_mins += round(dm * 60)
        control_dist_km -= dist
    return arrow_time.shift(hours=+delta_hrs, minutes=+delta_mins).isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    arrow_time = None
    try:
        arrow_time = arrow.get(brevet_start_time)
    except:
        arrow_time = arrow.now()
    if brevet_dist_km not in regulation_close_times:
        brevet_dist_km = 200 # Default to 200
    control_dist_km = max(control_dist_km, 0) # Positive
    delta_hrs, delta_mins = 0, 0 # Time addition measured in hours and minutes

    if 0 <= control_dist_km <= 60:
        # Need French calculation of 20 km/h
        dm, dh = math.modf(control_dist_km / 20)
        delta_hrs += 1 + dh
        delta_mins += round(dm * 60)
    elif control_dist_km >= brevet_dist_km:
        # Assume that it's the end
        delta_hrs = regulation_close_times[brevet_dist_km]
    else: 
        for ms in min_speeds:
            # (location range, speed)
            dist = min(ms[0], control_dist_km)
            dm, dh = math.modf(dist / ms[1])
            delta_hrs += dh
            delta_mins += round(dm * 60)
            control_dist_km -= dist
    return arrow_time.shift(hours=+delta_hrs, minutes=+delta_mins).isoformat()
